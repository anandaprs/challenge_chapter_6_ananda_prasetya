const { UserType } = require('../models');

class userTypeRepository {
  static findAll() {
    return UserType.findAll();
  }
  static find(id) {
    return UserType.findByPk(id);
  }
}

module.exports = userTypeRepository;