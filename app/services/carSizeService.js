const carSizeRepository = require('../repositories/carSizeRepository');

class carSizeService {
  static getAll() {
    return carSizeRepository.findAll();
  }
  static get(id) {
    return carSizeRepository.find(id);
  }
}

module.exports = carSizeService;